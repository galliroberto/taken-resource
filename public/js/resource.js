const params = new URLSearchParams(window.location.search)

const resource = {
    typology: params.get("resource_typology") || "post",
    id: params.get("resource_id") || 5,
}
const user = {
    id: params.get("id") || 1,
    username: params.get("username") || "gerard",
}

const socket = io("ws://localhost:3000/resource", {
    reconnection: true,
    transports: ["websocket"],
})


socket.on("connect", () => {
    initLog()

    takeResource(resource, user)

    function takeResource(resource, user) {
        socket.emit("take-resource", resource, user)
    }
})

socket.on("resource-status", (data) => {
    appendLog(JSON.stringify(data))
})


socket.on("connect_error", (err) => {
    console.log(err instanceof Error)
    console.log(err.message)
    console.log(err.data)
})


window.addEventListener("beforeunload", function (e) {
    //e.preventDefault()
    //e.returnValue = ' '
})

window.addEventListener("unload", function (e) {
    socket.disconnect()
})

function appendLog(data) {
    const node = document.createElement("p")
    const textnode = document.createTextNode(data)
    node.appendChild(textnode)

    document.getElementById("log").appendChild(node)
}


function initLog() {
    document.getElementById("info").innerHTML =
        `socket: ${socket.id}\nresource: ${JSON.stringify(resource)}\nuser: ${JSON.stringify(user)}`

    document.getElementById("log").innerText = ''
}