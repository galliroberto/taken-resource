const params = new URLSearchParams(window.location.search)

const resource = {
    typology: params.get("resource_typology") || "post",
}

const user = {
    id: params.get("id") || 1,
    username: params.get("username") || "benjamin",
}


const socket = io("ws://localhost:3000/resources", {
    reconnection: true,
    transports: ["websocket"],
})

socket.on("connect", () => {
    initLog()

    getTakenResources(resource)

    function getTakenResources(resource) {
        socket.emit("resources-taken", resource)
    }
})

socket.on("resources-status", (data) => {
    appendLog(JSON.stringify(data))

    updateResourcesStatus(data)

    function updateResourcesStatus(data) {
        initTable()

        console.log('pecchiè', data)

        data.forEach((item) => {
            let row = document.getElementById(item.resource.typology + "-" + item.resource.id)

            row.cells[2].innerHTML = `<b>${item.user.username}</b>`
        })
    }
})


socket.on("connect_error", (err) => {
    console.log(err instanceof Error)
    console.log(err.message)
    console.log(err.data)
})



function initLog() {
    document.getElementById("info").innerHTML =
        `Hey Bob!!!\nsocket: ${socket.id}\nresource: ${JSON.stringify(resource)}`

    document.getElementById("log").innerText = ""
}

function appendLog(data) {
    const node = document.createElement("p")
    const textnode = document.createTextNode(data)
    node.appendChild(textnode)

    document.getElementById("log").appendChild(node)
}

function initTable() {
    const table_body = document.getElementById("table-body")
    table_body.innerText = ""
    for (id in [...Array(10).keys()]) {
        let row = table_body.insertRow(id)
        row.setAttribute("id", resource.typology + "-" + id, 0)

        row.insertCell(0).appendChild(document.createTextNode(id))
        row
            .insertCell(1)
            .appendChild(document.createTextNode(resource.typology + "-" + id))
        row.insertCell(2).appendChild(document.createTextNode("..."))
    }
}
