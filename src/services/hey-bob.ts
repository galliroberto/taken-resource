import {Server, Socket} from "socket.io";
import {Resource} from "../interfaces/resource";
import {User} from "../interfaces/user";
import {Payload} from "../interfaces/payload";
import {ResourcesStatus} from "./resource-status";

const resourcesStatus: ResourcesStatus = new ResourcesStatus('map')

class HeyBob {
    io: Server
    socket: Socket
    resource: Resource
    resourceUnique: string

    constructor(io: Server, socket: Socket, resource: Resource) {
        this.io = io;
        this.socket = socket;
        this.resource = resource;
    }

    /**
     * get list of taken resources by resource typology
     */
    async getTakenResourcesByTypology() {
        const socketsInResourceRoom = this._getSocketsInResourceRoomByTypology();

        if (undefined === socketsInResourceRoom) {
            return []
        }

        return await Promise.all(
            [...socketsInResourceRoom.values()].map(async socketId => await resourcesStatus.get(socketId))
        )
    }

    private _getSocketsInResourceRoomByTypology(): Set<any> {
        return this.io.of('/resource').adapter.rooms.get(this.resource.typology)
    }
}


export class HeyBobResources extends HeyBob {
    constructor(io: Server, socket: Socket, resource: Resource) {
        super(io, socket, resource);
    }

    joinToResourceRoom(): void {
        this.socket.join(this.resource.typology)
    }

    /**
     * notify to the list client open resources the list of busy resources by resource typology
     */
    async notifyMeTakenResourcesStatusByTypology(): Promise<void> {
        this.socket.emit("resources-status", await super.getTakenResourcesByTypology());
    }
}


export class HeyBobResource extends HeyBob {
    user: User
    payload: any

    constructor(io: Server, socket: Socket, resource: Resource, user: User) {
        super(io, socket, resource);

        this.user = user;
        this.resourceUnique = this.resource.typology + ':' + this.resource.id
    }

    /**
     * check if resource is free
     */
    canITakeResource(): boolean {
        const socketsInResourceRoom = this._getSocketsInResourceRoomByUnique()
        return 0 === (undefined !== socketsInResourceRoom ? socketsInResourceRoom.size : 0)
    }

    /**
     * set the resource taken
     */
    ITakeResource(): void {
        this.socket.join(this.resource.typology)
        this.socket.join(this.resourceUnique)

        resourcesStatus.set(this.socket.id, this._makeSocketPayload())
    }

    /**
     * notify to the resource client the resource status
     */
    async notifyMeResourceStatus() {
        this.socket.emit("resource-status", await resourcesStatus.get(this._getSocketIdHasTakenResource()));
    }

    /**
     * check if the resource is taken by me
     */
    isResourceTakenByMe(): boolean {
        const socketId = this._getSocketIdHasTakenResource()
        if (null === socketId) {
            return false
        }
        return socketId === this.socket.id
    }

    /**
     * set to free the resource taked by me
     */
    unTakenResource(): void {
        this.socket.leave(this.resourceUnique)
        this.socket.leave(this.resource.typology)

        resourcesStatus.delete(this.socket.id)

        // emtpy room unique if two concurrency taken it
        const socketsInResourceRoom = this._getSocketsInResourceRoomByUnique()
        if (undefined !== socketsInResourceRoom && socketsInResourceRoom.size > 0) {
            ([...socketsInResourceRoom.values()]).forEach(async socketId => {
                if (this.io.sockets.sockets.has(socketId)) {
                    this.io.sockets.sockets.get(socketId).leave(this.resourceUnique)
                }
                if(await resourcesStatus.has(socketId)) {
                    resourcesStatus.delete(socketId)
                }
            })
        }
    }

    /**
     * notify to all the list clients the status of resources
     */
    async notifyStatusToResourceRoom(): Promise<void> {
        this.io.of('/resources').in(this.resource.typology).emit('resources-status', await super.getTakenResourcesByTypology())
    }

    /**
     * set payload to store and retry later
     */
    private _makeSocketPayload(): Payload {
        return {
            socket: {
                id: this.socket.id
            },
            resource: this.resource,
            user: this.user
        }
    }

    private _getSocketIdHasTakenResource(): string {
        const socketsInResourceRoom = this._getSocketsInResourceRoomByUnique()
        if (undefined === socketsInResourceRoom) {
            return null
        }

        return socketsInResourceRoom.values().next().value
    }

    private _getSocketsInResourceRoomByUnique(): Set<any> {
        return this.io.of('/resource').adapter.rooms.get(this.resourceUnique)
    }

}


module.exports = {HeyBobResource, HeyBobResources}