import {Payload} from "../interfaces/payload";

const {promisify} = require('util')

export class ResourcesStatus {
    private _provider: ResourceStatusProvider;

    constructor(provider: 'map' | 'redis') {
        switch (provider) {
            case 'redis':
                this._provider = new RedisResourcesStatusProvider()
                break
            case 'map':
            default:
                this._provider = new MapResourcesStatusProvider()
        }
    }

    async all(): Promise<Array<Payload>> {
        return await this._provider.all()
    }

    async set(k: string, v: Payload): Promise<void> {
        await this._provider.set(k, v)
    }

    async has(k: string): Promise<boolean> {
        return await this._provider.has(k)
    }

    async get(k: string): Promise<Payload> {
        return await this._provider.get(k)
    }

    delete(k: string): void {
        this._provider.delete(k);
    }
}

abstract class ResourceStatusProvider {
    abstract all(): Promise<Array<Payload>>

    abstract set(k: string, v: Payload): Promise<void>

    abstract has(k: string): Promise<boolean>

    abstract get(k: string): Promise<Payload>

    abstract delete(k: string): void
}

class MapResourcesStatusProvider extends ResourceStatusProvider {
    private static _instance: MapResourcesStatusProvider
    private archive: Map<any, any> = new Map()

    constructor() {
        super()
        if (MapResourcesStatusProvider._instance) {
            return MapResourcesStatusProvider._instance
        }
        MapResourcesStatusProvider._instance = this;
    }

    async all(): Promise<Array<Payload>> {
        return [...this.archive.values()]
    }

    async set(k: string, v: Payload): Promise<void> {
        await this.archive.set(k, v)
    }

    async has(k: string): Promise<boolean> {
        return await this.archive.has(k)
    }

    async get(k: string): Promise<Payload> {
        return await this.archive.get(k)
    }

    delete(k: string): void {
        this.archive.delete(k);
    }
}

class RedisResourcesStatusProvider extends ResourceStatusProvider {
    private client: any;
    private amget: any;
    private aget: any;
    private aset: any;
    private ahas: any;
    private akeys: any;
    private avalues: any;

    constructor() {
        super();

        const redis = require("redis");
        this.client = redis.createClient({host: 'ws_redis', db: 9});

        this.akeys = promisify(this.client.keys).bind(this.client)
        this.amget = promisify(this.client.mget).bind(this.client)
        this.aget = promisify(this.client.get).bind(this.client)
        this.aset = promisify(this.client.set).bind(this.client)
        this.ahas = promisify(this.client.exists).bind(this.client)
    }

    async all(): Promise<Array<Payload | []>> {
        const keys = await this.akeys("*")
        if (0 === keys.length){
            return []
        }

        return (await this.amget(await this.akeys("*"))).map((obj: string) => JSON.parse(obj));
    }

    async set(k: string, v: Payload): Promise<void> {
        await this.aset(k, JSON.stringify(v));
    }

    async has(k: string): Promise<boolean> {
        return await this.ahas(k)
    }

    async get(k: string): Promise<Payload> {
        return JSON.parse(await this.aget(k));
    }

    delete(k: string): void {
        this.client.del(k);
    }
}


