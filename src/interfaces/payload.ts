import {Resource} from "./resource";
import {User} from "./user";

export interface Payload {
    socket: {
        id: string
    },
    resource: Resource,
    user: User
}