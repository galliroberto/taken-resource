export interface Resource {
    id: string,
    typology: string
}