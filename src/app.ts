import {ResourcesStatus} from "./services/resource-status";

const port = 3000
const express = require('express');
const app = express();
const http = require('http').Server(app);

//import {createServer} from "http";
import {Server, Socket} from "socket.io";

// const httpServer = createServer();
const io = new Server(http, {
    transports: ["websocket"]
});

app.use(express.static('public'));

import {Resource} from "./interfaces/resource";
import {User} from "./interfaces/user";
import {HeyBobResource, HeyBobResources} from './services/hey-bob'

/*
io.of('/').on('connection', (socket: Socket) => {
    console.log('really connection income ' + socket.id)
    socket.emit('daje')

    socket.on('message', (data) => {
        console.log('in-message');
        socket.emit('Pong ' + data)
    })
    socket.on('error', (data) => {
        socket.emit('Error ' + data)
    })
})
*/

io.of('/resources').on('connection', (socket: Socket) => {
    socket.on("resources-taken", (resource: Resource) => {
        const heyBob = new HeyBobResources(io, socket, resource)

        heyBob.joinToResourceRoom()
        heyBob.notifyMeTakenResourcesStatusByTypology()
    })
})


io.of('/resource').on('connection', (socket: Socket) => {
    socket.on("take-resource", (resource: Resource, user: User) => {
        const heyBob = new HeyBobResource(io, socket, resource, user)

        if (heyBob.canITakeResource()) {
            heyBob.ITakeResource()
            heyBob.notifyStatusToResourceRoom()
        }

        heyBob.notifyMeResourceStatus()

        socket.on("disconnecting", (reason: string) => {
            if (heyBob.isResourceTakenByMe()) {
                heyBob.unTakenResource()
                heyBob.notifyStatusToResourceRoom()
            }

            console.log('disconnecting reason', reason)
        });

        /*
        socket.on("disconneted", (reason: string) => {
            console.log('disconneted reason', reason)
        });

        socket.onAny((event, ...args) => {
            //console.log(`debug event ${event}`);
        });
         */
    });
});


exports.server = http.listen(port, () => {
    console.log('listening on *:' + port);
});








