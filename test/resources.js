'use strict'

const ResourcesStatus = require("../dist/services/resource-status")

console.log = function () {
};

const {assert, expect} = require('chai')
const {server} = require('../dist/app')
const io = require('socket.io-client')
const io_options = {
    transports: ['websocket'],
    forceNew: true,
    reconnection: true
}

var socket_a
var socket_b
var socket_z

const resource_a = {
    typology: 'post',
    id: 'a'
}
const resource_b = {
    typology: 'post',
    id: 'b'
}
const user_a = {
    id: 1,
    username: 'arnold'
}
const user_b = {
    id: 2,
    username: 'silvester'
}
const user_z = {
    id: 777,
    username: 'mago-zurli'
}


describe('Resources Event', function () {

    before(done => {
        server.start
        done()
    })

    after(done => {
        //server.close()
        done()
    })

    beforeEach(function (done) {
        socket_a = io('http://localhost:3000/resource', io_options)
        socket_b = io('http://localhost:3000/resource', io_options)
        socket_z = io('http://localhost:3000/resources', io_options)

        done()
    })
    afterEach(function (done) {
        socket_a.disconnect()
        socket_b.disconnect()
        socket_z.disconnect()

        done()
    })

    describe('Clients observe resources', function () {

        it('Receive empty resources taken if nobody takes one', function (done) {
            socket_z.emit('resources-taken', resource_a)
            socket_z.on('resources-status', function (data) {

                assert.deepEqual([], data)

                done()
            })
        })

        it('Receive one resource taken when someone takes one', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_z.emit('resources-taken', resource_a)
            socket_z.once('resources-status', function (data) {

                expect(1).to.equal(data.length)
                assert.deepEqual(socket_a.id, data[0].socket.id)
                assert.deepEqual(resource_a, data[0].resource)
                assert.deepEqual(user_a, data[0].user)

                done()
            })
        })

        it('Receive resource taken by my first socket_id when I taken it by two socket_id', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_b.emit('take-resource', resource_a, user_a)
            socket_z.emit('resources-taken', resource_a)
            socket_z.once('resources-status', async function (data) {

                expect(1).to.equal(data.length)
                assert.deepEqual(socket_a.id, data[0].socket.id)
                assert.deepEqual(resource_a, data[0].resource)
                assert.deepEqual(user_a, data[0].user)

                done()
            })
        })
    })
})