'use strict'


console.log = function () {};

const {assert, expect} = require('chai')
const {server} = require('../dist/app')
const io = require('socket.io-client')
const io_options = {
    transports: ['websocket'],
    forceNew: true,
    reconnection: true
}

var socket_a
var socket_b

const resource_a = {
    typology: 'post',
    id: 'a'
}
const resource_b = {
    typology: 'post',
    id: 'b'
}
const user_a = {
    id: 1,
    username: 'arnold'
}
const user_b = {
    id: 2,
    username: 'silvester'
}


describe('Resource Event', function () {

    before(done => {
        server.start
        done()
    })

    after(done => {
        //server.close()
        done()
    })

    beforeEach(function (done) {
        socket_a = io('http://localhost:3000/resource', io_options)
        socket_b = io('http://localhost:3000/resource', io_options)

        done()
    })
    afterEach(function (done) {
        socket_a.disconnect()
        socket_b.disconnect()

        done()
    })

    describe('Clients try to take resource', function () {

        it('Receive resource taken by socket_id when it is un-taken', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_a.on('resource-status', function (data) {

                assert.deepEqual(socket_a.id, data.socket.id)
                assert.deepEqual(user_a, data.user)

                done()
            })
        })

        it('Receive resource taken by other when try to re-taken it', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_b.emit('take-resource', resource_a, user_a)
            socket_b.on('resource-status', function (data) {

                assert.deepEqual(socket_a.id, data.socket.id)
                assert.deepEqual(user_a, data.user)

                done()
            })
        })

        it('Receive resource taken by other when it is already taken', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)

            socket_b.emit('take-resource', resource_a, user_b)
            socket_b.on('resource-status', function (data) {

                assert.deepEqual(socket_a.id, data.socket.id)
                assert.deepEqual(user_a, data.user)

                done()
            })
        })

        it('Receive resource taken by my socket_id when it is un-taken by other', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_a.disconnect()

            socket_b.emit('take-resource', resource_a, user_b)
            socket_b.on('resource-status', function (data) {

                assert.deepEqual(socket_b.id, data.socket.id)
                assert.deepEqual(user_b, data.user)

                done()
            })
        })

        it('Receive resource taken by my first socket_id when I taken it by two socket_id', function (done) {
            socket_a.emit('take-resource', resource_a, user_a)
            socket_b.emit('take-resource', resource_a, user_a)
            socket_b.on('resource-status', function (data) {

                assert.deepEqual(socket_a.id, data.socket.id)
                assert.deepEqual(user_a, data.user)

                done()
            })
        })

    })
})