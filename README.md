CAN I TAKE RESOURCE?
===

The aim of this project is to avoid the concurrency editing of resources.


First of all configure docker, then start it
```
cp docker-compose.override.yml.dist docker-compose.override.yml

docker-compose up
```

How to try it:
- open browser
- set url parameters to try different resource / user
  - resource_typology (default post)
  - resource_id (default 5) [from 0 to 9]
  - user_id (default 1)
  - username (default gerard)
  
to monitor resources status   
```
http://localhost:PORT_IN_DOCKER_COMPOSE_OVERRIDE/resources.html
```

to take resource
```
http://localhost:PORT_IN_DOCKER_COMPOSE_OVERRIDE/resource.html
http://localhost:PORT_IN_DOCKER_COMPOSE_OVERRIDE/resource.html?username=jonnhy
http://localhost:PORT_IN_DOCKER_COMPOSE_OVERRIDE/resource.html?username=bob&resource_id=7
```


### Test

```
docker-compose run --rm ws_node /bin/sh -c "npm run test -- --coverage"
```


#### How to check websocket
```
# port is open?
netstat -nap |grep 3000

# install tool to test websocket
npm install -g wscat

# exec tool
wscat -c "ws://localhost:3000/socket.io/?EIO=4&transport=websocket"

wscat -c "ws://localhost:3000/socket.io/?EIO=4&transport=websocket" -w 1 -x '{"action": "helloWorld"}'

```



#### Benchmark

https://gist.github.com/zhangjiayin/81c0ac24a1e8984e046f


https://www.phoenixframework.org/blog/the-road-to-2-million-websocket-connections
https://esl-conf-staging.s3.eu-central-1.amazonaws.com/esl-conf-stg/media/files/000/000/080/original/the-road-to-2-mil.pdf?1462973768
https://kemalcr.com/blog/2016/11/13/benchmarking-and-scaling-websockets-handling-60000-concurrent-connections/
https://cppcomet.readthedocs.io/en/latest/load%20testing/Stress%20testing/
https://gist.github.com/georgeOsdDev/9910615

```
# install
sudo apt-get update && apt-get install tsung -y

# sample config 
cd /usr/share/doc/tsung/examples

sudo mkdir -p /usr/local/lib/tsung
sudo cp /usr/lib/x86_64-linux-gnu/tsung/bin/tsung_stats.pl /usr/local/lib/tsung/bin/tsung_stats.pl

# exec
tsung -k -f benchmark/tsung.xml start

# dashboard
http://localhost:8091/
```


### dependcy injection
https://nehalist.io/dependency-injection-in-typescript/
https://medium.com/seventyseven/typescript-dependency-injection-the-easy-way-83f6e94c476b